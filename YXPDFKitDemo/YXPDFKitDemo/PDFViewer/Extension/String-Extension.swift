//
//  String-Extension.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/26.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import Foundation

extension String {
    
    var isBlank: Bool {
        let trimmedStr = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedStr.isEmpty
    }
    
}
