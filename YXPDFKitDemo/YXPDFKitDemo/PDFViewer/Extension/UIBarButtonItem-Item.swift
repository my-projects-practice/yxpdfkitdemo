//
//  UIBarButtonItem-Item.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/22.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import Foundation
import UIKit

extension UIBarButtonItem {
    
    convenience init(imageName: String, highImageName: String, target: AnyObject, action: Selector) {
        
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: imageName), for: .normal)
        btn.setImage(UIImage(named: highImageName), for: .highlighted)
        btn.sizeToFit()
        btn.addTarget(target, action: action, for: .touchUpInside)
        let containView = UIView(frame: btn.bounds)
        containView.addSubview(btn)
        
        self.init(customView: containView)
    }
    
    convenience init(imageName: String, selImageName: String, target: AnyObject, action: Selector) {
        
        let btn = UIButton(type: .custom)
        btn.setImage(UIImage(named: imageName), for: .normal)
        btn.setImage(UIImage(named: selImageName), for: .selected)
        btn.sizeToFit()
        btn.addTarget(target, action: action, for: .touchUpInside)
        let containView = UIView(frame: btn.bounds)
        containView.addSubview(btn)
        
        self.init(customView: containView)
    }
    
}
