//
//  YXPDFThumbnailView.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/21.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit
import PDFKit

public class YXPDFThumbnailView: PDFThumbnailView {
    
    init?(pdfView: PDFView?) {
        super.init(frame: CGRect.zero)
        
        self.pdfView = pdfView
        thumbnailSize = CGSize(width: kPDFThumbnailSizeWidth, height: kPDFThumbnailSizeHeight)
        layoutMode = .horizontal
        backgroundColor = UIColor.lightGray
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
