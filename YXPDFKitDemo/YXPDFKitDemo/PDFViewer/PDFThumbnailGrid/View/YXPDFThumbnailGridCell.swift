//
//  YXPDFThumbnailGridCell.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/22.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit

class YXPDFThumbnailGridCell: UICollectionViewCell {
    
    open var image: UIImage? = nil {
        didSet {
            thumbnailImageView.image = image
        }
    }
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var pageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

}
