//
//  YXPDFThumbnailGridViewController.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/22.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit
import PDFKit

protocol ThumbnailGridViewControllerDelegate: class{
    func thumbnailGridViewController(_ thumbnailGridViewController: YXPDFThumbnailGridViewController, didSelectPage page: PDFPage)
}

class YXPDFThumbnailGridViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private let YXPDFThumbnailGridCellId = "YXPDFThumbnailGridCell"
    private let cols: Int = 2
    private let margin: CGFloat = 10
    
    open var pdfDocument: PDFDocument?
    weak var delegate: ThumbnailGridViewControllerDelegate?
    
    convenience init?(pdfDocument: PDFDocument?, delegate target: ThumbnailGridViewControllerDelegate?) {
        
        guard pdfDocument != nil else { return nil }
        
        let layout = UICollectionViewFlowLayout()
        self.init(collectionViewLayout: layout)
        
        // CollectionView Layout
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumInteritemSpacing = kPDFThumbnailCollectionViewMinimumInteritemSpacing
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing = kPDFThumbnailCollectionViewMinimumLineSpacing
        let width = (view.frame.size.width - margin * CGFloat((cols + 1))) / CGFloat(cols)
        let height = width * 1.5
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize = CGSize(width: width, height: height)
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).sectionInset = UIEdgeInsets(top: CGFloat(kPDFThumbnailCollectionViewInsetTop), left: CGFloat(kPDFThumbnailCollectionViewInsetLeft), bottom: kPDFThumbnailCollectionViewInsetBottom, right: kPDFThumbnailCollectionViewInsetRight)
        
        collectionView!.register(UINib.init(nibName: "YXPDFThumbnailGridCell", bundle: nil), forCellWithReuseIdentifier: YXPDFThumbnailGridCellId)
        collectionView!.backgroundColor = UIColor.gray
        self.pdfDocument = pdfDocument
        delegate = target
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        initCollectionView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes

        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
}

// MARK: UICollectionViewDataSource
extension YXPDFThumbnailGridViewController {

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return pdfDocument?.pageCount ?? 0
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: YXPDFThumbnailGridCellId, for: indexPath) as! YXPDFThumbnailGridCell
        
        if let page = pdfDocument?.page(at: indexPath.item) {
            let thumbnail = page.thumbnail(of: cell.bounds.size, for: .cropBox)
            cell.image = thumbnail
            cell.pageLabel.text = page.label
        }
    
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension YXPDFThumbnailGridViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let page = pdfDocument?.page(at: indexPath.item) {
//            dismiss(animated: false, completion: nil)
            delegate?.thumbnailGridViewController(self, didSelectPage: page)
        }
    }
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
}
