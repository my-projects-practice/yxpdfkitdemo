//
//  Const.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/22.
//  Copyright © 2020 Jin Xin. All rights reserved.
//
import Foundation
import UIKit

let UIScreenH: CGFloat = UIScreen.main.bounds.height

let kPDFThumbnailSizeWidth: CGFloat = 30
let kPDFThumbnailSizeHeight: CGFloat = 30

let kPDFThumbnailCollectionViewMinimumInteritemSpacing: CGFloat = 10
let kPDFThumbnailCollectionViewMinimumLineSpacing: CGFloat = 20

let kPDFThumbnailCollectionViewInsetTop: CGFloat = 10
let kPDFThumbnailCollectionViewInsetLeft: CGFloat = 10
let kPDFThumbnailCollectionViewInsetBottom: CGFloat = 10
let kPDFThumbnailCollectionViewInsetRight: CGFloat = 10

let kPDFSearchBarHeight: CGFloat = 56
let kPDFSearchResultBarHeight: CGFloat = 50
let kPDFSearchViewCellHeight: CGFloat = 150

let kPDFPopoverPreferredContentWidth: CGFloat = 330
let kPDFPopoverShareHeight: CGFloat = 220
let kPDFPopoverCatalogHeight: CGFloat = (50 + 45 * 4)

// Notification
let kYXPDFToolBarThumbnailClickNotification: Notification.Name = Notification.Name(rawValue: "kYXPDFToolBarThumbnailClickNotification")
let kYXPDFToolBarAnnotationClickNotification: Notification.Name = Notification.Name(rawValue: "kYXPDFToolBarAnnotationClickNotification")
let kYXPDFToolBarSearchClickNotification: Notification.Name = Notification.Name(rawValue: "kYXPDFToolBarSearchClickNotification")
let kYXPDFToolBarShareClickNotification: Notification.Name = Notification.Name(rawValue: "kYXPDFToolBarShareClickNotification")
