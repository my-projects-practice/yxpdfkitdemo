//
//  YXPDFSearchViewCell.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/24.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit

class YXPDFSearchViewCell: UITableViewCell {

    @IBOutlet weak var pdfPageImageView: UIImageView!
    @IBOutlet weak var pageNumLabel: UILabel!
    @IBOutlet weak var searchResultTextLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
