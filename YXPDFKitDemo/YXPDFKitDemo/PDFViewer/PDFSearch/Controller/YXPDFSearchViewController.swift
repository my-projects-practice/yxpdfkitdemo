//
//  YXPDFSearchViewController.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/25.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit
import PDFKit

protocol SearchViewControllerDelegate: class {
    
    func searchViewController(_ searchViewController: YXPDFSearchViewController, didSelectSearchResult selection: PDFSelection)
    
    func searchViewController(_ searchViewController: YXPDFSearchViewController, searchCacheText text: String)
}

let YXPDFSearchViewCellId = "YXPDFSearchViewCell"

class YXPDFSearchViewController: UIViewController {
    
    var pdfDocument: PDFDocument?
    
    weak var delegate: SearchViewControllerDelegate?
    
    lazy var searchResults = [PDFSelection]()
    lazy var searchChangedResults = [PDFSelection]()
    
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 0, height: kPDFSearchBarHeight))
        searchBar.delegate = self
        searchBar.searchBarStyle = .minimal
        searchBar.placeholder = "搜索文档"
        searchBar.becomeFirstResponder()
        
        return searchBar
    }()
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: CGRect.zero, style: .plain)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = kPDFSearchViewCellHeight
        tableView.register(UINib(nibName: "YXPDFSearchViewCell", bundle: nil), forCellReuseIdentifier: YXPDFSearchViewCellId)
        
        return tableView
    }()
    
    lazy var searchResultCountLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 1
        label.backgroundColor = UIColor.lightGray
        label.isHidden = true
        
        return label
    }()
    
    init(pdfDocument: PDFDocument!, delegate target: SearchViewControllerDelegate?, searchCacheText text: String?, popoverPresentationView sourceView: UIView) {
        super.init(nibName: nil, bundle: nil)
        
        // PDFDocument
        self.pdfDocument = pdfDocument
        pdfDocument?.delegate = self
        
        // Controller Delegate
        delegate = target
        
        // Search Cache Text
        searchBar.text = text
        
        // Popover
        modalPresentationStyle = .popover
        popoverPresentationController?.sourceView = sourceView
        popoverPresentationController?.sourceRect = CGRect.null
        popoverPresentationController?.permittedArrowDirections = .any
        popoverPresentationController?.backgroundColor = UIColor.white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        layoutUI()
    }
    
}

// MARK: - Table view data source
extension YXPDFSearchViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: YXPDFSearchViewCellId, for: indexPath) as! YXPDFSearchViewCell
        
        let selection = searchResults[indexPath.row]
        let page = selection.pages.first
        
        // Page Image
        let pageImage = page?.thumbnail(of: CGSize(width:60, height: 80), for: .cropBox)
        
        // Page Number
        let pageStr = page?.label ?? ""
        let pageTitleText = "页" + pageStr
        
        // Page Text
        let extendSelection = selection.copy() as! PDFSelection
        extendSelection.extend(atStart: 10)
        extendSelection.extend(atEnd: 90)
        extendSelection.extendForLineBoundaries()
        let range = (extendSelection.string! as NSString).range(of: selection.string!, options: .caseInsensitive)
        let attrResultText = NSMutableAttributedString(string: extendSelection.string!)
        attrResultText.addAttribute(.backgroundColor, value: UIColor.yellow, range: range)
        
        cell.pdfPageImageView.image = pageImage
        cell.pageNumLabel.text = pageTitleText
        cell.searchResultTextLabel.attributedText = attrResultText
        
        return cell
    }

}

// MARK: Setup UI and Layout UI
extension YXPDFSearchViewController {
    
    private func setupUI() {
        view.addSubview(tableView)
        view.addSubview(searchResultCountLabel)
    }
    
    private func layoutUI() {
        preferredContentSize = CGSize(width: kPDFPopoverPreferredContentWidth, height: kPDFSearchBarHeight)
        
        tableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.height.greaterThanOrEqualTo(kPDFSearchBarHeight)
            make.bottom.equalToSuperview().offset(0)
        }

        searchResultCountLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.bottom.equalToSuperview().offset(0)
            make.height.equalTo(kPDFSearchResultBarHeight)
        }
    }
    
    // refresh search UI and update UI layout
    private func refreshSearchResults(isEmpty: Bool) {
        DispatchQueue.main.async { [weak self] in
            // refresh table view and search result count
            self!.searchResults = self!.searchChangedResults
            self!.tableView.reloadData()
            
            if isEmpty == true {
                self!.preferredContentSize = CGSize(width: kPDFPopoverPreferredContentWidth, height: self!.searchBar.bounds.height)
                
                self!.tableView.snp.updateConstraints { (make) in
                    make.bottom.equalToSuperview().offset(0)
                }
                
                self!.searchResultCountLabel.isHidden = true
                
            } else {
                self!.searchResultCountLabel.text = String(format: "发现 %d 个匹配结果", self!.searchResults.count)
                
                // adjust popover content size height
                var height: CGFloat
                
                if self!.searchResults.count == 0 { // search does not match
                    height = self!.searchBar.bounds.height + self!.searchResultCountLabel.bounds.height
                
                } else { // search match
                    height = UIScreenH
                
                }
                
                self!.preferredContentSize = CGSize(width: kPDFPopoverPreferredContentWidth, height: height)
                
                self!.searchResultCountLabel.isHidden = false
                
                self!.tableView.snp.updateConstraints { (make) in
                    make.bottom.equalToSuperview().offset(-kPDFSearchResultBarHeight)
                }
            }
        }
    }
    
}

// MARK: - Table view delegate
extension YXPDFSearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return searchBar
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return kPDFSearchBarHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selection = searchResults[indexPath.row]
        delegate?.searchViewController(self, didSelectSearchResult: selection)
        dismiss(animated: false, completion: nil)
    }
    
}

// MARK: ScrollView Delegate
extension YXPDFSearchViewController {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        searchBar.resignFirstResponder()
    }
    
}

// MARK: UISearchBarDelegate
extension YXPDFSearchViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        // isEmpty
        if searchBar.searchTextField.text!.isEmpty {
            searchChangedResults.removeAll()
            refreshSearchResults(isEmpty: true)
            searchBar.becomeFirstResponder()
            
        } else {
            refreshSearchResults(isEmpty: false)
        }
        
        pdfDocument?.cancelFindString()
        pdfDocument?.beginFindString(searchBar.searchTextField.text!, withOptions: .caseInsensitive)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        pdfDocument?.cancelFindString()
        dismiss(animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        pdfDocument?.cancelFindString()
        // isEmpty
        if searchText.isEmpty {
            searchChangedResults.removeAll()
            refreshSearchResults(isEmpty: true)
        }
        
        pdfDocument?.beginFindString(searchText, withOptions: .caseInsensitive)
        
        delegate?.searchViewController(self, searchCacheText: searchText)
    }
    
}

// MARK: PDFDocumentDelegate
extension YXPDFSearchViewController: PDFDocumentDelegate {
    
    func didMatchString(_ instance: PDFSelection) {
        searchChangedResults.append(instance)
    }
    
//    func documentDidFindMatch(_ notification: Notification) {
//        print("documentDidFindMatch")
//    }
    
    func documentDidBeginDocumentFind(_ notification: Notification) {
        searchChangedResults.removeAll()
    }
    
    func documentDidEndDocumentFind(_ notification: Notification) {
        refreshSearchResults(isEmpty: false)
    }
    
//    func documentDidUnlock(_ notification: Notification) {
//        print("PDF documentDidUnlock() \(Thread.current)")
//    }
}
