//
//  YXPDFSearchNavigationController.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/27.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit

class YXPDFSearchNavigationController: UINavigationController {
    
    init(rootViewController: UIViewController, popoverPresentationView sourceView: UIView) {
        super.init(rootViewController: rootViewController)
        
        modalPresentationStyle = .popover
        popoverPresentationController?.sourceView = sourceView
        popoverPresentationController?.sourceRect = CGRect.null
//        popoverPresentationController?.passthroughViews = [sourceView]
        popoverPresentationController?.permittedArrowDirections = .any
        popoverPresentationController?.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
}
