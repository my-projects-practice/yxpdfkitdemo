//
//  YXPDFViewController.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/21.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit
import PDFKit
import SnapKit

class YXPDFViewController: UIViewController {
    
    @IBOutlet weak var pdfDocumentContentView: UIView!
    @IBOutlet weak var pdfBottomBar: UIView!
    @IBOutlet weak var seperatorLine: UIView!
    
    var pdfFileName: String?
    
    var pdfView: YXPDFView?
    
    var pdfThumbnailGridView: UICollectionView?
    
    lazy var onceSetupPdfThumbnailGrid: Void = {
        setupPdfThumbnailGrid()
    }()
    
    // Native ThumbnailView
    lazy var thumbnailView: YXPDFThumbnailView? = {
        guard let thumbnailView = YXPDFThumbnailView(pdfView: pdfView) else { return nil }
        return thumbnailView
    }()
    
    var searchCacheText: String?
    
    // Constructor
    static func instantiate(pdfFileName: String?) -> YXPDFViewController {
        let pdfVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: String(describing: self)) as! YXPDFViewController
        pdfVc.pdfFileName = pdfFileName
        
        return pdfVc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        layoutUI()
        registerNotification()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}

// MARK: Setup ViewController
extension YXPDFViewController {
    
    // Setup UI
    private func setupUI() {
        // Navigation Bar
        setupNavigationBar()
        
        // PDFDocumentView
        setupPdfDocumentView()
        
        // PDFThumbnailView
        pdfBottomBar.addSubview(thumbnailView!)
    }
    
    private func setupNavigationBar() {
        title = pdfFileName
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(imageName: "icon-close", highImageName: "icon-close", target: self, action: #selector(cancelPdfView))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: YXPDFToolBar.initWithDocument())
    }
    
    private func setupPdfDocumentView() {
        let documentVc = YXPDFDocumentViewController(pdfFileName: pdfFileName!)
        addChild(documentVc)
        pdfDocumentContentView.addSubview(documentVc.pdfView!)
        pdfView = documentVc.pdfView!
    }
    
    private func setupPdfThumbnailGrid() {
        guard let thumbnailGridVc = YXPDFThumbnailGridViewController(pdfDocument: pdfView!.document, delegate: self) else {
            print("setupPdfThumbnailGrid() failed.")
            return
        }
        addChild(thumbnailGridVc)
        pdfThumbnailGridView = thumbnailGridVc.collectionView
    }
    
    
    // Layout UI
    private func layoutUI() {
        pdfView!.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.bottom.equalTo(pdfBottomBar.snp.top)
        }
        
        thumbnailView!.snp.makeConstraints { (make) in
            make.top.equalTo(seperatorLine.snp.bottom)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.bottom.equalToSuperview().offset(0)
        }
    }
    
    
    // Register Notification
    private func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(thumbnailClickAction), name: kYXPDFToolBarThumbnailClickNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(annotationClickAction), name: kYXPDFToolBarAnnotationClickNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(searchClickAction(notify:)), name: kYXPDFToolBarSearchClickNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(shareClickAction(notify:)), name: kYXPDFToolBarShareClickNotification, object: nil)
    }
        
}

// MARK: NavigationItem Actions
extension YXPDFViewController {
    
    @objc private func cancelPdfView() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: Notification Actions
extension YXPDFViewController {
    
    // Thumbnail
    @objc func thumbnailClickAction() {
        let count = pdfDocumentContentView.subviews.count
        if pdfDocumentContentView.subviews[count-1].isMember(of: YXPDFView.self) {
            _ = onceSetupPdfThumbnailGrid
            pdfDocumentContentView.addSubview(pdfThumbnailGridView!)
            pdfBottomBar.isHidden = true
        } else {
            pdfDocumentContentView.addSubview(pdfView!)
            pdfBottomBar.isHidden = false
        }
    }
    
    // Annotation
    @objc func annotationClickAction() {
        print("annotationClickAction")
    }
    
    // Search
    @objc func searchClickAction(notify: Notification) {
        let sourceView = notify.userInfo!["searchSourceView"] as! UIButton
        let searchVc = YXPDFSearchViewController(pdfDocument: pdfView?.document, delegate: self, searchCacheText: searchCacheText, popoverPresentationView: sourceView)
        
        present(searchVc, animated: true, completion: nil)
    }
    
    // Share
    @objc func shareClickAction(notify: Notification) {
        let sourceView = notify.userInfo!["shareSourceView"] as! UIButton
        let shareNavVc = YXPDFShareNavigationController.initWithStoryboard(pdfView: pdfView!, sourcePresentationView: sourceView)
        
        present(shareNavVc, animated: true, completion: nil)
    }
    
}

// MARK: ThumbnailGridViewControllerDelegate
extension YXPDFViewController: ThumbnailGridViewControllerDelegate {
    
    func thumbnailGridViewController(_ thumbnailGridViewController: YXPDFThumbnailGridViewController, didSelectPage page: PDFPage) {
        thumbnailClickAction()
        pdfView!.go(to: page)
    }
    
}

// MARK: SearchViewControllerDelegate
extension YXPDFViewController: SearchViewControllerDelegate {
    
    func searchViewController(_ searchViewController: YXPDFSearchViewController, didSelectSearchResult selection: PDFSelection) {
        
        func switchPdfDocumentView() {
            let count = pdfDocumentContentView.subviews.count
            if !pdfDocumentContentView.subviews[count-1].isMember(of: YXPDFView.self) {
                pdfDocumentContentView.addSubview(pdfView!)
                pdfBottomBar.isHidden = false
            }
        }
        
        switchPdfDocumentView()
        selection.color = UIColor.yellow
        pdfView?.currentSelection = selection
        pdfView?.go(to: selection)
    }
    
    func searchViewController(_ searchViewController: YXPDFSearchViewController, searchCacheText text: String) {
        searchCacheText = text
    }
}
