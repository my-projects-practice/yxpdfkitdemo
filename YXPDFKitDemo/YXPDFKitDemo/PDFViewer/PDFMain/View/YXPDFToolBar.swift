//
//  YXPDFToolBar.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/22.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit
import PDFKit

class YXPDFToolBar: UIView {
    
    var pdfDocument: PDFDocument?
//    var pdfMainViewController: UIViewController?
    
    class func initWithDocument() -> YXPDFToolBar{
        let toolBar = Bundle.main.loadNibNamed(String(describing: self), owner: self, options:nil)!.first as! YXPDFToolBar
//        toolBar.pdfMainViewController = pdfMainViewController
//        toolBar.pdfDocument = pdfDocument
        
        return toolBar
    }
    
}

// MARK: PDF Tool Bar Actions
extension YXPDFToolBar {
    
    @IBAction func thumbnailBtnClick(_ sender: UIButton) {
        NotificationCenter.default.post(Notification.init(name: kYXPDFToolBarThumbnailClickNotification))
    }
    
    @IBAction func annotationBtnClick(_ sender: UIButton) {
        print("annotationBtnClick")
        NotificationCenter.default.post(name: kYXPDFToolBarAnnotationClickNotification, object: nil, userInfo: nil)
    }
    
    @IBAction func searchBtnClick(_ sender: UIButton) {
        NotificationCenter.default.post(name: kYXPDFToolBarSearchClickNotification, object: nil, userInfo: ["searchSourceView": sender])
    }
    
    @IBAction func shareBtnClick(_ sender: UIButton) {
        NotificationCenter.default.post(name: kYXPDFToolBarShareClickNotification, object: nil, userInfo: ["shareSourceView": sender])
    }
    
}
