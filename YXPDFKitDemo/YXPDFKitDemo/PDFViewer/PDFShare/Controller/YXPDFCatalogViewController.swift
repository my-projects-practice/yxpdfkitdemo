//
//  YXPDFCatalogViewController.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/27.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit
import PDFKit

class YXPDFCatalogViewController: UITableViewController {

    @IBOutlet weak var currentPageLabel: UILabel!
    @IBOutlet weak var selectPagesLabel: UILabel!
    @IBOutlet weak var totlePageLabel: UILabel!
    
    @IBOutlet weak var pickerTableViewCell: UITableViewCell!
    
    var pdfView: PDFView?
    
    @IBOutlet weak var selImage0: UIImageView!
    @IBOutlet weak var selImage1: UIImageView!
    @IBOutlet weak var selImage3: UIImageView!
    @IBOutlet weak var selImage4: UIImageView!
    
    lazy var selImageArr: [UIImageView] = {
        return [selImage0, selImage1, UIImageView(), selImage3, selImage4]
    }()
    
    var prevImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = selImageArr
        prevImageView = selImage4
        
        currentPageLabel.text = String(format: "当前页 (%@)", pdfView!.currentPage!.label!)
//        currentPageLabel.textColor = UIColor.lightGray
        totlePageLabel.text = String(format: "全部(%d页)", pdfView!.document!.pageCount)
        
        preferredContentSize = CGSize(width: kPDFPopoverPreferredContentWidth, height: kPDFPopoverCatalogHeight + 170)
//        tableView.snp.updateConstraints { (make) in
//            make.bottom
//        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

}

extension YXPDFCatalogViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pdfView!.document!.pageCount
    }
    
}

// MARK: UIPickerViewDelegate
extension YXPDFCatalogViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(row + 1)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let comp1Row = pickerView.selectedRow(inComponent: 0)
        var comp2Row = pickerView.selectedRow(inComponent: 1)
        if comp1Row > comp2Row {
            pickerView.selectRow(comp1Row, inComponent: 1, animated: true)
            comp2Row = comp1Row
        }
        
        if comp1Row == comp2Row {
            selectPagesLabel.text = String(format: "第 %d 页", comp1Row+1)
        } else {
            selectPagesLabel.text = String(format: "第 %d-%d 页", comp1Row+1, comp2Row+1)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 35
    }
    
}

// MARK: UITableViewDelegate
extension YXPDFCatalogViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 2 {
//            if pickerTableViewCell.isHidden == false {
//                pickerTableViewCell.isHidden = true
//            }
            
            if prevImageView! == selImageArr[indexPath.row] as UIImageView {
                
            } else {
                prevImageView!.isHidden = true
                let imageView = selImageArr[indexPath.row] as UIImageView
                imageView.isHidden = false
                prevImageView = imageView
            }
        }
        
        if indexPath.row == 1 {
            if pickerTableViewCell.isHidden {
                pickerTableViewCell.isHidden = false
                selectPagesLabel.textColor = UIColor.systemGreen
                preferredContentSize = CGSize(width: kPDFPopoverPreferredContentWidth, height: kPDFPopoverCatalogHeight+170)
//                tableView.frame = CGRect(x: 0, y: 0, width: kPDFPopoverPreferredContentWidth, height: kPDFPopoverCatalogHeight + 150)
//                tableView.superview!.frame = tableView.frame
//                tableView.layoutIfNeeded()
                print("1 super frame: \(tableView.superview!.frame)")
                print("1 super bounds: \(tableView.superview!.bounds)")

                print("1 frame:\(tableView.frame)")
                print("1 bounds:\(tableView.bounds)")
                print("1 preferredContentSize: \(preferredContentSize)")

            } else {
                pickerTableViewCell.isHidden = true
                selectPagesLabel.textColor = UIColor.black
                preferredContentSize = CGSize(width: kPDFPopoverPreferredContentWidth, height: kPDFPopoverCatalogHeight)
//                tableView.frame = CGRect(x: 0, y: 0, width: kPDFPopoverPreferredContentWidth, height: kPDFPopoverCatalogHeight)
//                tableView.superview!.frame = tableView.frame
//                tableView.layoutIfNeeded()
                print("1 super frame: \(tableView.superview!.frame)")
                print("1 super bounds: \(tableView.superview!.bounds)")
                
                print("2 frame:\(tableView.frame)")
                print("2 bounds:\(tableView.bounds)")
                print("2 preferredContentSize: \(preferredContentSize)")
            }
        } else if indexPath.row != 2 {
            if !pickerTableViewCell.isHidden {
                pickerTableViewCell.isHidden = true
                selectPagesLabel.textColor = UIColor.black
                preferredContentSize = CGSize(width: kPDFPopoverPreferredContentWidth, height: kPDFPopoverCatalogHeight)
                //                tableView.frame = CGRect(x: 0, y: 0, width: kPDFPopoverPreferredContentWidth, height: kPDFPopoverCatalogHeight)
                //                tableView.superview!.frame = tableView.frame
                //                tableView.layoutIfNeeded()
                print("1 super frame: \(tableView.superview!.frame)")
                print("1 super bounds: \(tableView.superview!.bounds)")
                
                print("2 frame:\(tableView.frame)")
                print("2 bounds:\(tableView.bounds)")
                print("2 preferredContentSize: \(preferredContentSize)")
            }
        }
        
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2 {
            if pickerTableViewCell.isHidden {
                return 0
            } else {
                return 150
            }
        }
        
        return 45
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 50
        }
        return 0
    }
    
}
