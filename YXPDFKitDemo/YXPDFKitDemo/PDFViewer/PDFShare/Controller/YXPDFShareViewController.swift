//
//  YXPDFShareViewController.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/27.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit
import PDFKit

class YXPDFShareViewController: UITableViewController {
    
    @IBOutlet weak var totalPageLabel: UILabel!
    
    var pdfView: PDFView?
//    class func initWithStoryboard() -> YXPDFShareViewController {
//        UIStoryboard(name: String(describing: self), bundle: nil).instantiateViewController(withIdentifier: String(describing: self)) as! YXPDFShareViewController
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        preferredContentSize = CGSize(width: kPDFPopoverPreferredContentWidth, height: kPDFPopoverCatalogHeight+150)
        totalPageLabel.text = String(format: "全部 (%d页)", pdfView!.document!.pageCount)
        totalPageLabel.textColor = UIColor.lightGray
    }

   
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVc = segue.destination as! YXPDFCatalogViewController
        destVc.pdfView = pdfView
    }
    
}

extension YXPDFShareViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1 && indexPath.row == 0 {
            print("分享")
            
//            NSString *shareTitle = @"分享的标题";
//                UIImage *shareImage = [UIImage imageNamed:@"me"];
//                NSURL *shareUrl = [NSURL URLWithString:@"https://www.jianshu.com/u/acdcce712303"];
//                NSArray *activityItems = @[shareTitle,
//                                           shareImage,
//                                           shareUrl]; // 必须要提供url 才会显示分享标签否则只显示图片
//                
//                
//                UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
//                
//                activityVC.excludedActivityTypes = [self excludetypes];
//                
//                activityVC.completionWithItemsHandler = ^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
//                    
//                    NSLog(@"activityType: %@,\ncompleted: %d,\nreturnedItems:%@,\nactivityError:%@",activityType,completed,returnedItems,activityError);
//                };
//                
//                [self presentViewController:activityVC animated:YES completion:nil];
//            }
//
//
//
//            -(NSArray *)excludetypes{
//                
//                NSMutableArray *excludeTypesM =  [NSMutableArray arrayWithArray:@[//UIActivityTypePostToFacebook,
//                                                                                  UIActivityTypePostToTwitter,
//                                                                                  UIActivityTypePostToWeibo,
//                                                                                  UIActivityTypeMessage,
//                                                                                  UIActivityTypeMail,
//                                                                                  UIActivityTypePrint,
//                                                                                  UIActivityTypeCopyToPasteboard,
//                                                                                  UIActivityTypeAssignToContact,
//                                                                                  UIActivityTypeSaveToCameraRoll,
//                                                                                  UIActivityTypeAddToReadingList,
//                                                                                  UIActivityTypePostToFlickr,
//                                                                                  UIActivityTypePostToVimeo,
//                                                                                  UIActivityTypePostToTencentWeibo,
//                                                                                  UIActivityTypeAirDrop,
//                                                                                  UIActivityTypeOpenInIBooks]];
//                
//                if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0) {
//                    [excludeTypesM addObject:UIActivityTypeMarkupAsPDF];
//                }
//                
//                return excludeTypesM;
        }
    }
}
