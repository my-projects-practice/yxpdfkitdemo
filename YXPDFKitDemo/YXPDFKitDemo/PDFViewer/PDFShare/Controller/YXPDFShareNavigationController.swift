//
//  YXPDFShareNavigationController.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/27.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit
import PDFKit

class YXPDFShareNavigationController: UINavigationController {
    
    var pdfView: PDFView?
    
    static func initWithStoryboard(pdfView: PDFView, sourcePresentationView sourceView: UIView) -> YXPDFShareNavigationController {
        let shareNavVc = UIStoryboard(name: "YXPDFShareViewController", bundle: nil).instantiateViewController(withIdentifier: String(describing: self)) as! YXPDFShareNavigationController
        
        shareNavVc.pdfView = pdfView
        
        shareNavVc.modalPresentationStyle = .popover
        shareNavVc.popoverPresentationController?.sourceView = sourceView
        shareNavVc.popoverPresentationController?.sourceRect = CGRect.null
        shareNavVc.popoverPresentationController?.permittedArrowDirections = .any
        shareNavVc.popoverPresentationController?.backgroundColor = UIColor.white
        
        return shareNavVc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        print(viewControllers)
        
        let shareVc = viewControllers[0] as! YXPDFShareViewController
        shareVc.pdfView = pdfView
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let destVc = segue.destination as! YXPDFShareViewController
//        destVc.pdfDocument = pdfDocument
//    }

}
