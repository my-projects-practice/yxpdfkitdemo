//
//  YXPDFDocumentViewController.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/23.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit

class YXPDFDocumentViewController: UIViewController {
    
    var pdfView: YXPDFView?
    
    convenience init(pdfFileName: String?) {
        self.init(nibName: nil, bundle: nil)
        self.pdfView = YXPDFView(pdfFileName: pdfFileName!)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
