//
//  YXPDFView.swift
//  YXPDFKitDemo
//
//  Created by Jin Xin on 2020/6/21.
//  Copyright © 2020 Jin Xin. All rights reserved.
//

import UIKit
import PDFKit

public class YXPDFView: PDFView {
    
    init?(pdfFileName: String) {
        super.init(frame: CGRect.zero)
        
        displayDirection = .horizontal
        usePageViewController(true)
        pageBreakMargins = UIEdgeInsets(top: 50, left: 20, bottom: 20, right: 50)
        autoScales = true
        backgroundColor = UIColor.gray
        
        guard let pdfFileUrl = Bundle.main.url(forResource: pdfFileName, withExtension: nil) else {
            print("PDF file is not found.")
            return nil
        }
        
        document = PDFDocument(url: pdfFileUrl)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
